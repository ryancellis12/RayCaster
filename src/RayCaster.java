import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.Buffer;


import javax.imageio.ImageIO;
import javax.imageio.stream.FileCacheImageInputStream;
import javax.swing.JFrame;
import javax.swing.JPanel;

import static java.lang.Math.max;
import static java.lang.Math.sqrt;

public class RayCaster extends JPanel {
    private Game game;
    public BufferedImage img;
    private InputHandler inputHandler;


    public void paint(Graphics g) {
        g.drawImage(img, 0, 0, 640, 480, this);
    }

    public RayCaster() {
        inputHandler = new InputHandler(this);
    }

    public static void main(String[] args) {
        RayCaster rayCaster = new RayCaster();
        rayCaster.img = new BufferedImage(640/4, 480/4, BufferedImage.TYPE_INT_RGB);
        rayCaster.game = new Game(640/4, 480/4, rayCaster.img, rayCaster.inputHandler);
        rayCaster.setPreferredSize(new Dimension(640, 480));
        JFrame frame = new JFrame("Ray Casting Game");

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(rayCaster, BorderLayout.CENTER);

        frame.setContentPane(panel);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setResizable(false);
        frame.addKeyListener(rayCaster.inputHandler);
        float time = 0;
        while (true) {
            // Wait 10ms
            try {
                Thread.sleep(10); // 1000 milliseconds is one second.
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }

            rayCaster.game.tick(System.nanoTime() - time);
            frame.repaint();
            time = System.nanoTime();
        }
    }

    public static class Game {
        public int width, height;
        public int map[][] =   {
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,1,0,1,0,1,0,0,0,1},
                {1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,0,0,0,1,0,0,0,1},
                {1,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,1,1,0,1,1,0,0,0,0,1,0,1,0,1,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,1,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1},
                {1,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1},
                {1,1,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1},
                {1,1,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1},
                {1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,1},
                {1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
                {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
        };

        public float posX = 22, posY = 12;  //x and y start position
        public float dirX = -1, dirY = 0; //initial direction vector
        public float planeX = 0, planeY = 0.66f; //the 2d raycaster version of camera plane

        public BufferedImage img;
        public BufferedImage wallTex;
        public BufferedImage floorTex;

        public InputHandler inputHandler;

        double time = 0; //time of current frame
        double oldTime = 0; //time of previous frame

        public Game(int w, int h, BufferedImage img, InputHandler inputHandler) {
            width = w;
            height = h;
            this.img = img;
            this.inputHandler = inputHandler;

            try {
                wallTex = ImageIO.read(Game.class.getResource("/wall.png"));
                floorTex = ImageIO.read(Game.class.getResource("/floor.png"));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void tick(float delta) {
            //update input and playerpos
            //timing for input and FPS counter

            oldTime = time;
            time = System.currentTimeMillis();
            double frameTime = (time - oldTime) / 1000.0; //frameTime is the time this frame has taken, in seconds
            //System.out.println(1.0 / frameTime); //FPS counter
            render();

            //speed modifiers
            double moveSpeed = frameTime * 5.0; //the constant value is in squares/second
            double rotSpeed = frameTime * 3.0; //the constant value is in radians/second
            inputHandler.tick();
            //move forward if no wall in front of you
            if (inputHandler.up.down)
            {
                if(map[(int)(posX + dirX * moveSpeed)][(int)(posY)] == 0) posX += dirX * moveSpeed;
                if(map[(int)(posX)][(int)(posY + dirY * moveSpeed)] == 0) posY += dirY * moveSpeed;
            }
            //move backwards if no wall behind you
            if (inputHandler.down.down)
            {
                if(map[(int)(posX - dirX * moveSpeed)][(int)(posY)] == 0) posX -= dirX * moveSpeed;
                if(map[(int)(posX)][(int)(posY - dirY * moveSpeed)] == 0) posY -= dirY * moveSpeed;
            }
            //rotate to the right
            if (inputHandler.right.down)
            {
                //both camera direction and camera plane must be rotated
                double oldDirX = dirX;
                dirX = (float) (dirX * Math.cos(-rotSpeed) - dirY * Math.sin(-rotSpeed));
                dirY = (float) (oldDirX * Math.sin(-rotSpeed) + dirY * Math.cos(-rotSpeed));
                double oldPlaneX = planeX;
                planeX = (float) (planeX * Math.cos(-rotSpeed) - planeY * Math.sin(-rotSpeed));
                planeY = (float) (oldPlaneX * Math.sin(-rotSpeed) + planeY * Math.cos(-rotSpeed));
            }
            //rotate to the left
            if (inputHandler.left.down)
            {
                //both camera direction and camera plane must be rotated
                double oldDirX = dirX;
                dirX = (float) (dirX * Math.cos(rotSpeed) - dirY * Math.sin(rotSpeed));
                dirY = (float) (oldDirX * Math.sin(rotSpeed) + dirY * Math.cos(rotSpeed));
                double oldPlaneX = planeX;
                planeX = (float) (planeX * Math.cos(rotSpeed) - planeY * Math.sin(rotSpeed));
                planeY = (float) (oldPlaneX * Math.sin(rotSpeed) + planeY * Math.cos(rotSpeed));
            }

        }

        /*
        Updates the image to render
         */
        private void render() {

            Graphics g = img.getGraphics();
            g.setColor(Color.GRAY);
            g.fillRect(0, 0, width, height);
            g.setColor(Color.DARK_GRAY);
            g.fillRect(0, 0, width, height/2);
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, width, height);

            for (int x = 0; x < width; x++) {
                double cameraX = 2f * x / width - 1f;
                double rayPosX = posX;
                double rayPosY = posY;
                double rayDirX = dirX + planeX * cameraX;
                double rayDirY = dirY + planeY * cameraX;

                //which box of the map we're in
                int mapX = (int) rayPosX;
                int mapY = (int) rayPosY;

                //length of ray from current position to next x or y-side
                double sideDistX;
                double sideDistY;

                //length of ray from one x or y-side to next x or y-side
                double deltaDistX = sqrt(1 + (rayDirY * rayDirY) / (rayDirX * rayDirX));
                double deltaDistY = sqrt(1 + (rayDirX * rayDirX) / (rayDirY * rayDirY));
                double perpWallDist;

                //what direction to step in x or y-direction (either +1 or -1)
                int stepX;
                int stepY;

                int hit = 0; //was there a wall hit?
                int side = 0; //was a NS or a EW wall hit?

                //calculate step and initial sideDist
                if (rayDirX < 0)
                {
                    stepX = -1;
                    sideDistX = (rayPosX - mapX) * deltaDistX;
                }
                else
                {
                    stepX = 1;
                    sideDistX = (mapX + 1.0 - rayPosX) * deltaDistX;
                }
                if (rayDirY < 0)
                {
                    stepY = -1;
                    sideDistY = (rayPosY - mapY) * deltaDistY;
                }
                else
                {
                    stepY = 1;
                    sideDistY = (mapY + 1.0 - rayPosY) * deltaDistY;
                }

                //perform DDA
                while (hit == 0)
                {
                    //jump to next map square, OR in x-direction, OR in y-direction
                    if (sideDistX < sideDistY)
                    {
                        sideDistX += deltaDistX;
                        mapX += stepX;
                        side = 0;
                    }
                    else
                    {
                        sideDistY += deltaDistY;
                        mapY += stepY;
                        side = 1;
                    }
                    //Check if ray has hit a wall
                    if (map[mapX][mapY] > 0) hit = 1;
                }

                //Calculate distance projected on camera direction (oblique distance will give fisheye effect!)
                if (side == 0) perpWallDist = (mapX - rayPosX + (1 - stepX) / 2) / rayDirX;
                else           perpWallDist = (mapY - rayPosY + (1 - stepY) / 2) / rayDirY;

                // perpWallDist = sqrt(perpWallDist) * perpWallDist;


                //Calculate height of line to draw on screen
                int lineHeight = (int)(height / perpWallDist);

                double br = 1 - perpWallDist/10;

                //calculate lowest and highest pixel to fill in current stripe
                int drawStart = -lineHeight / 2 + height / 2;
                if(drawStart < 0) drawStart = 0;
                int drawEnd = lineHeight / 2 + height / 2;
                if(drawEnd >= height) drawEnd = height - 1;

                //calculate value of wallX
                double wallX; //where exactly the wall was hit
                if (side == 0) wallX = rayPosY + perpWallDist * rayDirY;
                else           wallX = rayPosX + perpWallDist * rayDirX;
                wallX -= Math.floor(wallX);

                //x coordinate on the texture
                int texX = (int) (wallX * 16);
                if(side == 0 && rayDirX > 0) texX = 16 - texX - 1;
                if(side == 1 && rayDirY < 0) texX = 16 - texX - 1;

                //FLOOR CASTING
                double floorXWall, floorYWall; //x, y position of the floor texel at the bottom of the wall

                //4 different wall directions possible
                if(side == 0 && rayDirX > 0)
                {
                    floorXWall = mapX;
                    floorYWall = mapY + wallX;
                }
                else if(side == 0 && rayDirX < 0)
                {
                    floorXWall = mapX + 1.0;
                    floorYWall = mapY + wallX;
                }
                else if(side == 1 && rayDirY > 0)
                {
                    floorXWall = mapX + wallX;
                    floorYWall = mapY;
                }
                else
                {
                    floorXWall = mapX + wallX;
                    floorYWall = mapY + 1.0;
                }

                double distWall, distPlayer, currentDist;

                distWall = perpWallDist;
                distPlayer = 0.0;

                if (drawEnd < 0) drawEnd = height; //becomes < 0 when the integer overflows

                //draw the floor from drawEnd to the bottom of the screen
                for(int y = drawEnd; y < height; y++)
                {
                    currentDist = height / (2.0 * y - height); //you could make a small lookup table for this instead

                    double weight = (currentDist - distPlayer) / (distWall - distPlayer);

                    double currentFloorX = weight * floorXWall + (1.0 - weight) * posX;
                    double currentFloorY = weight * floorYWall + (1.0 - weight) * posY;

                    int floorTexX, floorTexY;
                    floorTexX = Math.abs((int)(currentFloorX * 16) % 16);
                    floorTexY = Math.abs((int)(currentFloorY * 16) % 16);
                    //floor
                    Color color = new Color(floorTex.getRGB(floorTexX, floorTexY));
                    img.setRGB(x, height - y - 1, color.getRGB());
                    img.setRGB(x, y, color.getRGB());
                }

                double br2 = br;
                int dh = (int) ((height/2) - (height / 25.0));
                for(int y = 0; y < (height/2); y++)
                {
                    Color color = new Color(img.getRGB(x, y));
                    br2 = 1 + ((double) -y / dh);
                    color = new Color(Math.max((int) (color.getRed() * br2), 0),
                            Math.max((int) (color.getGreen() * br2),0),
                            Math.max((int) (color.getBlue() * br2),0));
                    img.setRGB(x, height - y - 1, color.getRGB());
                    img.setRGB(x, y, color.getRGB());
                }

                for(int y = drawStart; y<=drawEnd; y++)
                {
                    int d = y * 256 - height * 128 + lineHeight * 128;  //256 and 128 factors to avoid floats
                    int texY = ((d*15) / lineHeight-1) / 256;
                    // int texY = (15*(y-(-lineHeight / 2 + height / 2)))/(lineHeight-1);
                    texY = Math.abs(texY);
                    texY = Math.min(15, texY);
                    Color color = new Color(wallTex.getRGB(texX, texY));
                    color = new Color(Math.max((int) (color.getRed() * br), 0),Math.max((int) (color.getGreen() * br),0),Math.max((int) (color.getBlue() * br),0));
                    if (side == 0) color = color.darker().darker();
                    img.setRGB(x, y, color.getRGB());
                }

//                Color col = Color.LIGHT_GRAY;
//                if (side == 1) col = col.darker();

//                g.setColor(col);
//                g.drawLine(x, drawStart, x, drawEnd);
            }
        }

    }
}
